from rest_framework import viewsets
from rest_framework.response import Response

from blog import models, serializers


class BlogViewSet(viewsets.ViewSet):
    def list(self, request):
        try:
            page = models.BlogIndexPage.objects.live().first()
            data = serializers.BlogIndexSerializer(page).data
        except models.BlogIndexPage.DoesNotExist:
            return Response(status=404)
        return Response(data)

    def retrieve(self, request, pk=None):
        try:
            page = models.BlogPage.objects.live().get(pk=pk)
            data = serializers.BlogPostSerializer(page).data
        except models.BlogPage.DoesNotExist:
            return Response(status=404)
        return Response(data)
