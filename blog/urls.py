from rest_framework import routers

from blog.views import BlogViewSet

router = routers.SimpleRouter()
router.register(r"blogs", BlogViewSet, basename="blog")
