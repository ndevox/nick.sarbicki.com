from django.contrib.syndication.views import Feed

from blog.models import BlogPage


class BlogFeed(Feed):
    title = "Nick Sarbicki's Blog"
    link = "/blog/feed.xml"
    description = "All of my posts as they are published"

    def items(self):
        return BlogPage.objects.live().order_by("-first_published_at")

    def item_title(self, item):
        return item.title

    def item_description(self, item):
        return item.intro

    def item_tags(self, item):
        return item.tags.all()

    def item_link(self, item):
        return item.get_url()
