import hashlib
import json

from django.db import models
from modelcluster.contrib.taggit import ClusterTaggableManager
from modelcluster.fields import ParentalKey
from taggit.models import TaggedItemBase

from wagtail.admin.edit_handlers import StreamFieldPanel, FieldPanel, MultiFieldPanel
from wagtail.core import blocks
from wagtail.core.fields import StreamField
from wagtail.core.models import Page
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.images.models import Image

from wagtailcodeblock.blocks import CodeBlock

from wagtailmaterialimage.blocks import MaterialImageBlock
from website.utils import serialize_body


class BlogPageTag(TaggedItemBase):
    content_object = ParentalKey("blog.BlogPage", related_name="tagged_posts")


class BlogIndexPage(Page):
    parent_page_types = ["home.HomePage"]
    subpage_types = ["blog.BlogPage"]

    intro = models.CharField(blank=True, max_length=128)
    content_panels = Page.content_panels + [FieldPanel("intro", classname="full")]

    api_fields = [
        "intro",
    ]

    @property
    def posts(self):
        return self.get_children().live().order_by("-first_published_at").specific()

    @property
    def json_posts(self):
        return [
            {
                "title": post.title,
                "intro": post.intro,
                "url": post.url,
                "published": post.first_published_at.isoformat(),
                "id": post.id,
            }
            for post in self.posts
        ]


class BlogPage(Page):
    parent_page_types = ["blog.BlogIndexPage"]
    subpage_types = []

    intro = models.CharField(blank=True, max_length=128)

    body = StreamField(
        [
            ("section", blocks.RichTextBlock()),
            ("quote", blocks.BlockQuoteBlock()),
            ("image", MaterialImageBlock()),
            ("code", CodeBlock()),
        ]
    )

    tags = ClusterTaggableManager(through=BlogPageTag, blank=True)

    feed_image = models.ForeignKey(
        Image, null=True, blank=True, on_delete=models.SET_NULL, related_name="+"
    )

    content_panels = Page.content_panels + [
        FieldPanel("intro", classname="full"),
        StreamFieldPanel("body"),
        FieldPanel("tags"),
    ]

    promote_panels = [
        MultiFieldPanel(Page.promote_panels, "Common page configuration"),
        ImageChooserPanel("feed_image"),
    ]

    api_fields = [
        "intro",
        "body",
        "tags",
        "first_published_at",
    ]

    @property
    def json_data(self):

        data = {
            "title": self.title,
            "intro": self.intro,
            "body": serialize_body(self.body),
        }

        return json.dumps(data)

    @property
    def author_avatar(self):
        return "//www.gravatar.com/avatar/{}?s=200/".format(
            hashlib.md5(self.owner.email.lower().encode("utf-8")).hexdigest()
        )

    class Meta:
        ordering = ["first_published_at"]
