import hashlib

from rest_framework import serializers

from blog import models
from website.utils import serialize_body


class BlogIndexSerializer(serializers.ModelSerializer):
    posts = serializers.SerializerMethodField()

    def get_posts(self, object: models.BlogIndexPage):
        return [
            {
                "title": post.title,
                "intro": post.intro,
                "url": post.url,
                "published": post.first_published_at.isoformat(),
                "id": post.id,
            }
            for post in object.posts
        ]

    class Meta:
        model = models.BlogIndexPage

        fields = (
            "title",
            "intro",
            "posts",
        )


class BlogPostSerializer(serializers.ModelSerializer):

    author = serializers.SerializerMethodField()
    author_avatar = serializers.SerializerMethodField()
    body = serializers.SerializerMethodField()
    feed_img = serializers.SerializerMethodField()

    def get_author(self, object: models.BlogPage):
        return object.owner.get_full_name()

    def get_author_avatar(self, object: models.BlogPage):
        return "//www.gravatar.com/avatar/{}?s=200/".format(
            hashlib.md5(object.owner.email.lower().encode("utf-8")).hexdigest()
        )

    def get_body(self, object: models.BlogPage):
        return serialize_body(object.body)

    def get_feed_img(self, object: models.BlogPage):
        try:
            return object.feed_image.get_rendition("fill-320x200").url
        except AttributeError:  # Image doesn't exist, ignore it.
            return None

    class Meta:
        model = models.BlogPage
        fields = ("author", "author_avatar", "body", "intro", "feed_img", "title")
