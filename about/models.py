import json

from wagtail.admin.edit_handlers import StreamFieldPanel
from wagtail.core import blocks
from wagtail.core.fields import StreamField
from wagtail.core.models import Page
from wagtail.images.blocks import ImageChooserBlock

from wagtailcodeblock.blocks import CodeBlock

from website.utils import serialize_body


class AboutPage(Page):
    parent_page_types = ["home.HomePage"]
    subpage_types = []

    body = StreamField(
        [
            ("section", blocks.RichTextBlock()),
            ("quote", blocks.BlockQuoteBlock()),
            ("image", ImageChooserBlock()),
            ("code", CodeBlock()),
        ]
    )

    content_panels = Page.content_panels + [
        StreamFieldPanel("body"),
    ]

    @property
    def json_data(self):

        data = {"title": self.title, "body": serialize_body(self.body)}

        return json.dumps(data)
