# Generated by Django 2.0.2 on 2018-03-03 11:13

from django.db import migrations
import wagtail.core.blocks
import wagtail.core.fields
import wagtail.images.blocks


class Migration(migrations.Migration):

    dependencies = [
        ("about", "0002_auto_20180226_2003"),
    ]

    operations = [
        migrations.AlterField(
            model_name="aboutpage",
            name="body",
            field=wagtail.core.fields.StreamField(
                (
                    ("section", wagtail.core.blocks.RichTextBlock()),
                    ("quote", wagtail.core.blocks.BlockQuoteBlock()),
                    ("image", wagtail.images.blocks.ImageChooserBlock()),
                    (
                        "code",
                        wagtail.core.blocks.StructBlock(
                            (
                                (
                                    "language",
                                    wagtail.core.blocks.ChoiceBlock(
                                        choices=[
                                            ("bash", "Bash/Shell"),
                                            ("c", "C"),
                                            ("cpp", "C++"),
                                            ("css", "CSS"),
                                            ("django", "Django/Jinja2"),
                                            ("diff", "Diff"),
                                            ("docker", "Docker"),
                                            ("git", "Git"),
                                            ("go", "Go"),
                                            ("http", "HTML"),
                                            ("java", "Java"),
                                            ("javascript", "Javascript"),
                                            ("json", "JSON"),
                                            ("kotlin", "Kotlin"),
                                            ("nginx", "nginx"),
                                            ("python", "Python"),
                                            ("r", "R"),
                                            ("jsx", "React JSX"),
                                            ("sass", "Sass (Sass)"),
                                            ("scss", "Sass (Scss)"),
                                            ("sql", "SQL"),
                                            ("swift", "Swift"),
                                            ("typescript", "TypeScript"),
                                            ("yaml", "YAML"),
                                        ],
                                        help_text="Coding language",
                                        label="Language",
                                    ),
                                ),
                                ("code", wagtail.core.blocks.TextBlock(label="Code")),
                            )
                        ),
                    ),
                )
            ),
        ),
    ]
