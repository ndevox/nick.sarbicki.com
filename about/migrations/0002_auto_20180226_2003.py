# Generated by Django 2.0.2 on 2018-02-26 20:03

from django.db import migrations
import wagtail.core.blocks
import wagtail.core.fields
import wagtail.images.blocks


class Migration(migrations.Migration):

    dependencies = [
        ("about", "0001_initial"),
    ]

    operations = [
        migrations.AlterField(
            model_name="aboutpage",
            name="body",
            field=wagtail.core.fields.StreamField(
                (
                    ("section", wagtail.core.blocks.RichTextBlock()),
                    ("quote", wagtail.core.blocks.BlockQuoteBlock()),
                    ("image", wagtail.images.blocks.ImageChooserBlock()),
                    (
                        "code",
                        wagtail.core.blocks.StructBlock(
                            (
                                (
                                    "language",
                                    wagtail.core.blocks.ChoiceBlock(
                                        choices=[
                                            ("bash", "Bash/Shell"),
                                            ("css", "CSS"),
                                            ("diff", "diff"),
                                            ("http", "HTML"),
                                            ("javascript", "Javascript"),
                                            ("json", "JSON"),
                                            ("python", "Python"),
                                            ("scss", "SCSS"),
                                            ("yaml", "YAML"),
                                        ],
                                        help_text="Coding language",
                                        label="Language",
                                    ),
                                ),
                                ("code", wagtail.core.blocks.TextBlock(label="Code")),
                            )
                        ),
                    ),
                )
            ),
        ),
    ]
