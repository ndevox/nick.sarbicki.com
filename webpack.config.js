const path = require("path");

module.exports = {
  context: __dirname,
  entry: {
    app: './assets/js/index'
  },
  output: {
      path: path.resolve('./assets/bundles/'),
      filename: "[name].js",
      chunkFilename: "[name]js"
  },

  module: {
      rules: [
        {
          test: /\.s?css$/,
          use: [
            { loader: "style-loader" },
            { loader: "css-loader" }
          ]
        },
        { test: /\.jsx?$/, exclude: /node_modules/, use: 'babel-loader' }
      ]
  },

  resolve: {
    modules: ['./node_modules'],
    extensions: ['.js', '.jsx']
  }
};
