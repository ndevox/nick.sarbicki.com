import json

from django.db import models
from wagtail.api import APIField

from wagtail.admin.edit_handlers import FieldPanel, StreamFieldPanel
from wagtail.core import blocks
from wagtail.core.fields import StreamField
from wagtail.core.models import Page
from wagtail.images.api.fields import ImageRenditionField

from wagtailcodeblock.blocks import CodeBlock

from wagtailmaterialimage.blocks import MaterialImageBlock, MaterialSlidesBlock
from website.utils import serialize_body


class ShowcaseIndexPage(Page):
    parent_page_types = ["home.HomePage"]
    subpage_types = ["showcase.Showcase"]

    intro = models.CharField(blank=True, max_length=128)
    content_panels = Page.content_panels + [FieldPanel("intro", classname="full")]

    @property
    def json_children(self):
        return json.dumps(
            [
                {
                    "id": showcase.id,
                    "title": showcase.title,
                    "url": showcase.url,
                    "background": showcase.background.get_rendition("fill-400x400").url,
                    "logo": showcase.logo.get_rendition("original").url,
                }
                for showcase in self.get_children().live().specific()
            ]
        )


class Showcase(Page):
    parent_page_types = ["showcase.ShowcaseIndexPage"]

    subpage_types = []

    intro = models.CharField(blank=True, max_length=64)

    logo = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="showcase_logo",
    )

    background = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="showcase_thumbnail",
    )

    body = StreamField(
        [
            ("section", blocks.RichTextBlock()),
            ("quote", blocks.BlockQuoteBlock()),
            ("image", MaterialImageBlock()),
            ("slides", MaterialSlidesBlock()),
            ("code", CodeBlock()),
        ]
    )

    content_panels = Page.content_panels + [
        FieldPanel("intro", classname="full"),
        FieldPanel("logo", classname="full"),
        FieldPanel("background", classname="full"),
        StreamFieldPanel("body"),
    ]

    api_fields = [
        "intro",
        APIField("logo", serializer=ImageRenditionField("original")),
        APIField("background", serializer=ImageRenditionField("original")),
        "body",
        "first_published_at",
    ]

    @property
    def json_data(self):

        return {
            "title": self.title,
            "intro": self.intro,
            "body": serialize_body(self.body),
        }
