from wagtail.core.blocks import ListBlock, StructBlock, TextBlock
from wagtail.images.blocks import ImageChooserBlock


class MaterialImageBlock(StructBlock):
    image = ImageChooserBlock()
    caption = TextBlock()

    class Meta:
        icon = "image"
        template = "wagtailmaterialimage/image_block.html"
        form_classname = "material-image-block struct-block"
        form_template = "wagtailmaterialimage/image_block_form.html"


class MaterialSlidesBlock(StructBlock):
    images = ListBlock(MaterialImageBlock)

    class Meta:
        icon = "image"
        template = "wagtailmaterialimage/slider_block.html"
        form_classname = "material-image-block struct-block"
        form_template = "wagtailmaterialimage/image_block_form.html"
