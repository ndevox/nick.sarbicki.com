import logging
import os

from bakery.views import BuildableMixin
from django.conf import settings
from django.test import RequestFactory
from wagtail.contrib.redirects.models import Redirect
from wagtail.contrib.sitemaps.views import sitemap
from wagtail.core.models import Site
from wagtail.documents.models import Document
from wagtailbakery.views import WagtailBakeryView

logger = logging.getLogger(__name__)


class SitemapBuildableView(BuildableMixin):

    build_path = "sitemap.xml"

    def get_site(self) -> Site:
        return Site.objects.get(is_default_site=True)

    def build(self):
        site = self.get_site()
        self.request = RequestFactory(SERVER_NAME=site.hostname).get(self.build_path)
        path = os.path.join(settings.BUILD_DIR, self.build_path)
        self.prep_directory(self.build_path)
        self.build_file(path, self.get_content())

    @property
    def build_method(self):
        return self.build

    def get_content(self):
        return sitemap(self.request).render().content


class RedirectBuildableView(WagtailBakeryView):
    queryset = Redirect.objects.all()

    def get_url(self, obj):
        return obj.old_path

    def get_path(self, obj):
        return obj.link

    def get_content(self, obj):
        response = self.get(self.request)
        response["Location"] = self.get_path(obj)
        return self.get_redirect_content(response, obj)

    def build_object(self, obj):
        """
        Build wagtail page and set SERVER_NAME to retrieve corresponding site
        object.
        """
        site = obj.site or Site.objects.get(is_default_site=True)
        logger.debug("Building %s" % obj)
        self.request = RequestFactory(SERVER_NAME=site.hostname).get(self.get_url(obj))
        self.set_kwargs(obj)
        path = self.get_build_path(obj)
        self.build_file(path, self.get_content(obj))


class DocumentBuildableView(WagtailBakeryView):
    queryset = Document.objects.all()

    def get_url(self, obj: Document):
        return obj.url

    def get_content(self, obj: Document):
        return obj.file.read()

    def get_build_path(self, obj: Document) -> str:
        url = self.get_url(obj)
        build_path = os.path.join(settings.BUILD_DIR, url[1:])
        build_dir = os.path.split(build_path)[0]
        os.path.exists(build_dir) or os.makedirs(build_dir)
        return build_path

    def build_object(self, obj):
        path = self.get_build_path(obj)
        self.build_file(path, self.get_content(obj))
