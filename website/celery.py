import logging
import os

from celery import Celery
from django.conf import settings
from django.core.management import call_command
from django.test import override_settings
from google.cloud import storage


logger = logging.getLogger(__name__)

app = Celery("nick.sarbicki.com")
app.config_from_object("django.conf:settings", namespace="CELERY")
app.autodiscover_tasks()


@app.task
def copy_to_gcloud(dir_name: str):
    client = storage.Client()
    bucket = client.get_bucket(settings.GS_BUCKET_NAME)
    for root, _, files in os.walk(dir_name):
        for file in files:
            file_name = os.path.join(root, file)

            upload_root = root

            if upload_root.startswith("build"):
                upload_root = root[5:]

            upload_path = os.path.join(upload_root, file).lstrip("/")
            logger.info(
                "Uploading %s to %s: %s",
                file_name,
                settings.GS_BUCKET_NAME,
                upload_path,
            )
            blob = bucket.blob(upload_path)
            blob.upload_from_filename(file_name)


@app.task
def run_build(**_kwargs):
    with override_settings(ALLOWED_HOSTS=["*"]):
        call_command("build")

    copy_to_gcloud.delay("build")
