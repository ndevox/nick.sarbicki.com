from __future__ import absolute_import, unicode_literals

from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ["*"]

EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"

SECRET_KEY = "NOT A REAL SECRET KEY"

BASE_URL = "http://localhost"

try:
    from .local import *
except ImportError:
    pass
