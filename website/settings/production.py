from __future__ import absolute_import, unicode_literals

from .base import *

DEBUG = False

ALLOWED_HOSTS = ["nick.sarbicki.com"]

DEFAULT_FILE_STORAGE = "website.settings.storage.WebsiteStorage"
STATICFILES_STORAGE = DEFAULT_FILE_STORAGE
COMPRESS_STORAGE = STATICFILES_STORAGE

GS_BUCKET_NAME = "nick.sarbicki.com"

COMPRESS_URL = "https://nick.sarbicki.com/"

STATIC_URL = COMPRESS_URL

try:
    from .local import *
except ImportError:
    pass
