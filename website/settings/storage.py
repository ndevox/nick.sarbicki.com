import os

from django.conf import settings
from storages.backends.gcloud import GoogleCloudStorage
from storages.utils import clean_name


class WebsiteStorage(GoogleCloudStorage):
    def url(self, name):
        name = self._normalize_name(clean_name(name))
        name = self._encode_name(name)

        return os.path.join(settings.COMPRESS_URL, name)
