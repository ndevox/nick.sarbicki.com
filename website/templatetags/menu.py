import json

from django.template import Library
from django.utils.safestring import mark_safe
from wagtail.core.models import Site

register = Library()


@register.simple_tag(takes_context=True)
def menu(context):

    # We are not expecting multiple sites so this should be enough
    root = Site.objects.first().root_page

    menu_pages = [root]
    menu_pages.extend(
        root.get_children()
        .live()
        .in_menu()
        .exclude(pk=context["page"].pk)
        .order_by("title")
    )

    menu_items = [
        {"url": page.url, "name": page.slug.capitalize() or page.title}
        for page in menu_pages
    ]

    return mark_safe(json.dumps(menu_items))
