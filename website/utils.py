import logging

from wagtail.core.fields import StreamField
from wagtail.core.rich_text import expand_db_html
from wagtail.core.signals import page_published

from website.celery import run_build


logger = logging.getLogger(__name__)


def serialize_body(body: StreamField):
    data = []
    for field in body:
        new_item = {"type": field.block_type, "id": field.id}

        if field.block_type == "section":
            new_item["value"] = expand_db_html(field.value.source)
        elif field.block_type == "image":
            new_item["value"] = {
                "image": field.value["image"].get_rendition("original").url,
                "caption": field.value["caption"],
            }
        elif field.block_type == "slides":
            new_item["value"] = [
                {
                    "image": image["image"].get_rendition("original").url,
                    "caption": image["caption"],
                }
                for image in field.value["images"]
            ]
        elif field.block_type == "code":
            new_item["value"] = dict(field.value)
        elif field.block_type == "quote":
            new_item["value"] = field.value
        else:
            new_item["value"] = field.block.get_api_representation(field.value)
        data.append(new_item)

    return data


def run_build_task(**_kwargs):
    logger.info("Running build task")
    run_build.delay()


page_published.connect(run_build_task, dispatch_uid="publish_build")
