FROM python:3.6-alpine
RUN apk add --update gcc libc-dev postgresql-dev nodejs yarn jpeg-dev zlib-dev
COPY . /code
WORKDIR /code
RUN yarn install && yarn run production
RUN pip install -r requirements.txt
