import PropTypes from 'prop-types';
import React from 'react';
import ReactDOM from 'react-dom';

import StreamBody from '../common/stream.body.jsx';


class AboutPage extends React.Component {

    render = () => {
        return (
            <div className="col offset-xl1 xl10 m12">
                <h1 className="title">{ this.props.title }</h1>
                <hr />
                <StreamBody data={this.props.body} />
            </div>
        )
    }
}

AboutPage.propTypes = {
    title: PropTypes.string,
    body: PropTypes.array
};

const renderAboutPage = (data) => {
    return ReactDOM.render(
        <AboutPage title={data.title} body={data.body} />,
        document.getElementById('about')
    )
};

window.renderAboutPage = renderAboutPage;
