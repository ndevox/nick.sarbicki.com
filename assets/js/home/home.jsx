import particles from 'exports-loader?particlesJS=window.particlesJS,window.pJSDom!particles.js';
import React from 'react';
import ReactDOM from 'react-dom';

import config from './particles.config';


class Home extends React.Component {
    loadParticles() {
      return particles.particlesJS(
        'home',
        config
      );
    }

    componentDidMount() {
        this.loadParticles();
    }

    render() {
        return (
            <div className="title-content">
                <h1>Nick Sarbicki</h1>
                <div className="row">
                    <a href={this.props.data['showcase_url']}>
                        {this.props.data['showcase_text']}
                    </a>
                    <a href={this.props.data['blog_url']}>
                        {this.props.data['blog_text']}
                    </a>
                    <a href={this.props.data['about_url']}>
                        {this.props.data['about_text']}
                    </a>
                </div>
            </div>
        );
    }
}


const renderHome = (data) => {
    ReactDOM.render(
        <Home data={data} />,
        document.getElementById('home')
    );
};

window.renderHome = renderHome;
