import PrismCode from 'react-prism';
import PropTypes from 'prop-types';
import React from 'react';
import LightBox from "./lightbox.jsx";
import Slides from './slider.jsx'



class StreamBody extends React.Component {
    renderSection(block){
        return <div key={block.id} className="block-section" dangerouslySetInnerHTML={ { __html: block.value } } />
    }

    renderImage(block){
        return (
            <div className="block-image" key={block.id}>
                <LightBox src={block.value.image} caption={block.value.caption} />
            </div>
        )
    }

    renderSlides(block){
        return (
            <div className="block-slides" key={block.id}>
                <Slides images={block.value} id={block.id} />
            </div>
        )
    }

    renderCode(block){
        return (
            <div className="block-code" key={block.id} >
                <PrismCode className={`language-${block.value.language}`} component="pre" >
                    {block.value.code}
                </PrismCode>
            </div>
        )
    }

    renderQuote(block){
        return (
            <div className="block-quote" key={block.id}>
                <blockquote>
                    {block.value}
                </blockquote>
            </div>
        )
    }

    renderBlock(block) {
        switch (block.type){
            case 'section':
                return this.renderSection(block);
            case 'image':
                return this.renderImage(block);
            case 'slides':
                return this.renderSlides(block);
            case 'code':
                return this.renderCode(block);
            case 'quote':
                return this.renderQuote(block);
        }
        return false
    }

    render() {
        return this.props.data.map((block) => this.renderBlock(block))
    }
}

StreamBody.propTypes = {
    data: PropTypes.array
};

export default StreamBody
