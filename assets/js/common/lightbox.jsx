import PropTypes from 'prop-types';
import React from 'react';

class LightBox extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            active: false
        }
    }

    onClick = () => {
        this.setState({
            active: !this.state.active
        })
    };

    componentDidUpdate = () => {
        if (this.state.active) {
            document.documentElement.style.overflowY = 'hidden';
        } else {
            document.documentElement.style.overflowY = 'auto';
        }
    };

    render = () => {
        return (
            <div className={this.state.active ? "lightbox active" : "lightbox"}
                 onClick={this.onClick}
                 tabIndex="0">
                <img src={this.props.src}/>
                <div>{this.props.caption}</div>
            </div>
        )
    }
}

LightBox.propTypes = {
    src: PropTypes.string,
    caption: PropTypes.string
};

export default LightBox
