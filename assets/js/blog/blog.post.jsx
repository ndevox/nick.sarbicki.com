import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';

import StreamBody from '../common/stream.body.jsx';


class BlogPost extends React.Component {
    renderBody() {
        return <StreamBody data={this.props.body} />
    }

    render() {
        return (
        <div className="col offset-xl1 xl10 m12">
            <h1 className="title center-align">{ this.props.title }</h1>
            <br />
            <h3>{ this.props.intro }</h3>
            <hr />
            <br />
            { this.renderBody() }
        </div>
        )
    }
}


const renderBlogPost = (data) => {
    return ReactDOM.render(
        <BlogPost title={data.title} intro={data.intro} body={data.body} />,
        document.getElementById('blogPost')
    )
};

BlogPost.propTypes = {
    intro: PropTypes.string,
    title: PropTypes.string,
    body: PropTypes.array
};


window.renderBlogPost = renderBlogPost;
