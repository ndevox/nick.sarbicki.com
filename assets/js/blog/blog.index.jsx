import { format } from 'date-fns';
import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import CSSTransition from 'react-transition-group/CSSTransition';


class BlogRow extends React.Component {

    state = {
        hidden: true
    };

    onClick = () => {
        this.setState({
            hidden: !this.state.hidden
        })
    };

    render = () => {
        return (
            <li className="blogRow">
              <div className="blogTitle" onClick={ this.onClick }>
                  <span className="blogTitleText">{ this.props.data.title }</span>
                  <span className="blogTitleDate">{ format(new Date(this.props.data.published), 'DD MMM YYYY') }</span>
              </div>
              <CSSTransition key={this.props.data.id} in={!this.state.hidden} timeout={500} classNames="intro">
                  <div className={this.state.hidden ? "blogIntro hidden" : "blogIntro"}>
                      <a href={this.props.data.url} className="row">
                          <span className="col s11">{this.props.data.intro}</span>
                          <div className="col s1">
                        <span className="btn-floating waves-effect waves-light">
                            <i className="material-icons">open_in_browser</i>
                        </span>
                          </div>
                      </a>
                  </div>
              </CSSTransition>
            </li>
        )
    }
}

BlogRow.propTypes = {
    data: PropTypes.object
};

class BlogIndex extends React.Component {

    renderPosts = () => {
        if(this.props.posts.length === 0) {
            return (
              <div className="progress offset-m2 m8">
                  <div className="indeterminate" />
              </div>
            )
        }

        return (
            <ul className="collapsible">
                {
                    this.props.posts.map(
                        post => (
                            <BlogRow key={post.title} data={post} />
                        )
                    )
                }
            </ul>
        )
    };

    render = () => {
        return (
            <div className="col offset-xl1 xl10 m12">
                <h1 className="title">{ this.props.title }</h1>
                <hr/>
                <h3>{ this.props.intro }</h3>
                <br/>
                <br/>
                { this.renderPosts() }
            </div>
        );
    }
}

BlogIndex.propTypes = {
    intro: PropTypes.string,
    title: PropTypes.string,
    posts: PropTypes.array,
};


const renderBlogIndex = (title, intro, posts) => {
    return ReactDOM.render(
        <BlogIndex title={title} intro={intro} posts={posts} />,
        document.getElementById('blogIndex')
    )
};


window.renderBlogIndex = renderBlogIndex;
