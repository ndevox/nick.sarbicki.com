import React from 'react';
import ReactDOM from 'react-dom';

import PropTypes from 'prop-types'


class ShowcaseItem extends React.Component {
    render = () => {
        return (
            <a href={this.props.data.url} className="col offset-l1 l5 s12 showcase-card">
                <img src={ this.props.data.background } className="showcase-background" />
                <img src={ this.props.data.logo } className="showcase-logo" />
                <div className="showcase-title">{ this.props.data.title }</div>
            </a>
        )
    }
}

ShowcaseItem.propTypes = {
    data: PropTypes.object
};

class ShowcaseIndex extends React.Component {
    render = () => {
        return (
            <div>
                <div className="col offset-m1 m11">
                    <h1 className="title">{ this.props.title }</h1>
                    <hr/>
                    <h3>{ this.props.intro }</h3>
                    <br/>
                    <br/>
                </div>
                {
                    this.props.showcases.map(
                        showcase => (
                            <ShowcaseItem key={showcase.id} data={showcase} />
                        )
                    )
                }
            </div>
        );
    }
}

ShowcaseIndex.propTypes = {
    intro: PropTypes.string,
    title: PropTypes.string,
    showcases: PropTypes.array
};


const renderShowcaseIndex = (title, intro, showcases) => {
    return ReactDOM.render(
        <ShowcaseIndex title={title} intro={intro} showcases={showcases} />,
        document.getElementById('showcaseIndex')
    )
};


window.renderShowcaseIndex = renderShowcaseIndex;
