from __future__ import absolute_import, unicode_literals

from django.db import models
from wagtail.core.models import Page


class HomePage(Page):

    about_text = models.CharField(default="About", max_length=10)
    about_url = models.URLField(default="/about/")
    blog_text = models.CharField(default="Blog", max_length=10)
    blog_url = models.URLField(default="/blog/")
    showcase_text = models.CharField(default="Showcase", max_length=10)
    showcase_url = models.URLField(default="/showcase/")

    parent_page_types = []
    subpage_types = [
        "about.AboutPage",
        "blog.BlogIndexPage",
        "showcase.ShowcaseIndexPage",
    ]

    api_fields = [
        "about_text",
        "about_url",
        "blog_text",
        "blog_url",
        "showcase_text",
        "showcase_url",
    ]

    @property
    def json_data(self):
        return {
            "about_text": self.about_text,
            "about_url": self.about_url,
            "blog_text": self.blog_text,
            "blog_url": self.blog_url,
            "showcase_text": self.showcase_text,
            "showcase_url": self.showcase_url,
        }
